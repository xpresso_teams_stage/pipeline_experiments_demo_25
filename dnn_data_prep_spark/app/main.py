"""
An example of how to use DataFrame for ML
"""
__author__ = "Naveen Sinha"


import sys
import argparse
import os
from pyspark.sql import SparkSession


if __name__ == "__main__":
    print('Running...', flush=True)

    parser = argparse.ArgumentParser()

    # Parse the arguments as give at the time of component deployment
    # extra/missing args will result in error and termination of this
    # program

    parser.add_argument('--arg-name-1', type=str, default='arg-value-1',
                        help='args 1 help')
    parser.add_argument('--arg-name-2', type=str, default='arg-value-2',
                        help='arg 2 help')
    parser.add_argument('--run_name', type=str, default='run_name-value',
                        help='run_name help')
    args = parser.parse_args()
    run_id = None
    if args:
        run_id = args.run_name
    print(f'All: {args}', flush=True)
    print(run_id, flush=True)

    spark = SparkSession \
        .builder \
        .getOrCreate()

    # Load an input file
    # input_path = 'path'
    # df = spark.read.format("parquet").load(input_path).cache()

    # Use your custom Transformer/Estimator with transfor/fit
    # df = CustomerTransformer().transform(df)

    # Save the records back in a parquet file.
    # output_loc = 'path'
    # print("Saving as Parquet file.")
    # df.write.parquet(output_loc)

    # Load the records back.

    spark.stop()
